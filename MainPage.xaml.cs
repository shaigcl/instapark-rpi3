﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Gpio;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.Core;
using Windows.Media.Playback;
using Windows.Networking;
using Windows.Networking.Sockets;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace App1
{
   
    public sealed partial class MainPage : Page
    {
        
        private const int LED_PIN1 = 17;
        private const int LED_PIN2 = 27;

        private const int BTN_PIN1 = 2;
        private const int BTN_PIN2 =3;
               

        private DispatcherTimer _timer;
        private DispatcherTimer blinkTimer;

        private int[] btnInputs = new int[] { 3, 4 };
        private int[] relayOutputs = new int[] { 17, 27 };

        private GpioPin[] btnPins = new GpioPin[2];
        private GpioPin[] relayPins = new GpioPin[2];

        private bool[] btnFlgs = new bool[2] { false, false };
        private bool[] chosenFlg = new bool[2] { false, false };
        private string status = "snap";
        private bool busyFlg = false;

        // private List<BitmapImage> seq = new List<BitmapImage>();
        //  DispatcherTimer seqTimer = new DispatcherTimer();

        private string deviceID = "0";
        private DatagramSocket listenerSocket1 = null;
        private DatagramSocket listenerSocket2 = null;

        const string sendPort = "7003";
        const string recievePort1 = "7001";
        const string recievePort2 = "7002";

        private MediaPlayer LoadSound = new MediaPlayer();
        private MediaPlayer ButtonSound1 = new MediaPlayer();
        private MediaPlayer ButtonSound2 = new MediaPlayer();
        private MediaPlayer CountDownSnap = new MediaPlayer();
        private MediaPlayer CountDownBoom = new MediaPlayer();
        private MediaPlayer ErrorSound = new MediaPlayer();
        private MediaPlayer ReleaseSound = new MediaPlayer();
        private MediaPlayer GetReadySound = new MediaPlayer();
             
        private MediaPlayer[] Sounds = new MediaPlayer[8];


        private SolidColorBrush greenBrush = new SolidColorBrush(Colors.LightGreen);
        private SolidColorBrush notChosenBrush = new SolidColorBrush(Colors.LightGray);

        #region ------------------------- INIT ----------------------------------------------

        public MainPage()
        {
            this.InitializeComponent();
            Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation eas =
    new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation();
            

            LoadSounds();

            if (InitGPIO())
                InitTimer();

            string name = System.Environment.MachineName;
            deviceID = name.Substring(name.Length - 1);
            txtDeviceID.Text = "Device ID: " + deviceID;
            Log(name);
            Listen();

            PlaySound(0); //Load
            BlinkLeds();

        }

        private bool InitGPIO()
        {
            var gpio = GpioController.GetDefault();

            if (gpio == null)
                return false;

            for (int i = 0; i < 2; i++) //Input - buttons
            {
                btnPins[i] = gpio.OpenPin(btnInputs[i]);
                btnPins[i].Write(GpioPinValue.High);
                btnPins[i].SetDriveMode(GpioPinDriveMode.Input);

            }

            for (int i = 0; i < 2; i++) //Output - Relays
            {
                relayPins[i] = gpio.OpenPin(relayOutputs[i]);
                relayPins[i].Write(GpioPinValue.High);
                relayPins[i].SetDriveMode(GpioPinDriveMode.Output);
            }

            return true;
        }

        private void InitTimer()
        {
            _timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(33)};

            _timer.Tick += (s, e) =>
            {
               
                if (!busyFlg)
                {
                    if (btnPins[0].Read() == GpioPinValue.Low)
                    {
                        if (!btnFlgs[0])
                        {
                            btnFlgs[0] = true;
                            ChooseLed(0, 1);

                            //chosenFlg[0] = true;
                            //chosenFlg[1] = false;
                            //Log("BTN 1 pressed");
                            //relayPins[0].Write(GpioPinValue.Low);
                            //relayPins[1].Write(GpioPinValue.High);
                            //PlaySound(1); //BTN1
                            //Send("BTN" + deviceID + "#snap");
                        }


                    }
                    else if (btnPins[0].Read() == GpioPinValue.High)
                    {
                        btnFlgs[0] = false;
                    }

                    if (btnPins[1].Read() == GpioPinValue.Low)
                    {
                        if (!btnFlgs[1])
                        {
                            btnFlgs[1] = true;
                            ChooseLed(1, 0);
                            
                            //chosenFlg[1] = true;
                            //chosenFlg[0] = false;
                            //Log("BTN 2 pressed");
                            //relayPins[1].Write(GpioPinValue.Low); //ON
                            //relayPins[0].Write(GpioPinValue.High); //OFF
                            //PlaySound(2); //BTN2
                            //Send("BTN" + deviceID + "#boom");
                        }

                    }
                    else if (btnPins[1].Read() == GpioPinValue.High)
                    {
                        btnFlgs[1] = false;
                    }

                }


            };


            _timer.Start();
            
        }

        private void ChooseLed(int btnOn,int btnOff)
        {
            chosenFlg[btnOn] = true;
            chosenFlg[btnOff] = false;
            Log("BTN " + (btnOn + 1).ToString() + " pressed");
            relayPins[btnOn].Write(GpioPinValue.Low);
            relayPins[btnOff].Write(GpioPinValue.High);
            PlaySound(btnOn + 1); //BTN1
            if (btnOn == 0)
            {
                Send("BTN#" + deviceID + "#snap");
                btnSnap.Background = greenBrush;
                btnBoom.Background = notChosenBrush;
                status = "snap";
            } else
            {
                Send("BTN#" + deviceID + "#boom");
                btnSnap.Background = notChosenBrush;
                btnBoom.Background = greenBrush;
                status = "boom";
            }
           
        }

        private void LoadSounds()
        {
            LoadSound.Source = MediaSource.CreateFromUri(new Uri("ms-appx:///Assets/Load.mp3"));
            ButtonSound1.Source = MediaSource.CreateFromUri(new Uri("ms-appx:///Assets/BTN1.mp3"));
            ButtonSound2.Source = MediaSource.CreateFromUri(new Uri("ms-appx:///Assets/BTN2.mp3"));
            CountDownSnap.Source = MediaSource.CreateFromUri(new Uri("ms-appx:///Assets/CountDownSnap.mp3"));
            CountDownBoom.Source = MediaSource.CreateFromUri(new Uri("ms-appx:///Assets/CountDownBoom.mp3"));
            ErrorSound.Source = MediaSource.CreateFromUri(new Uri("ms-appx:///Assets/Error.mp3"));
            ReleaseSound.Source = MediaSource.CreateFromUri(new Uri("ms-appx:///Assets/Release.mp3"));
            GetReadySound.Source = MediaSource.CreateFromUri(new Uri("ms-appx:///Assets/GetReady.mp3"));
          
            Sounds[0] = LoadSound;
            Sounds[1] = ButtonSound1;
            Sounds[2] = ButtonSound2;
            Sounds[3] = CountDownSnap;
            Sounds[4] = CountDownBoom;
            Sounds[5] = ErrorSound;
            Sounds[6] = ReleaseSound;
            Sounds[7] = GetReadySound;
           

            for (int i = 0; i < Sounds.Length; i++)
            {
                Sounds[i].AutoPlay = false;
            }

        }

        #endregion

        #region ------------------------- MANAGER ----------------------------------------------

        private void ParseMessage(string msg)
        {
            try
            {
                
                string[] parsed = msg.Split('#');

                if (parsed[0] == "STATUS")
                {
                    Send("STAT#" + deviceID + "#" + status);
                }

                if (parsed[1] == deviceID)
                {
                    txtLastMsg.Text = DateTime.Now + " - " + msg;
                    switch (parsed[0])
                    {
                        case "GetReady":
                            busyFlg = true;
                            PlaySound(7); //Get Ready
                            break;
                        case "SHOOT":
                            busyFlg = true;
                            if (parsed[2] == "snap")
                            {
                                PlaySound(3); //CountDown

                            }
                            else if (parsed[2] == "boom")
                            {
                                PlaySound(4); //CountDown
                            }


                            break;
                        case "ERROR":
                            PlaySound(5); //Error
                            break;
                        case "RELEASE":
                            busyFlg = false;
                            PlaySound(6); //Release
                            break;

                    }

                }
            }
            catch (Exception ex)
            {

                Log(ex.Message.ToString());
            }

        }

        #endregion

        #region =============================== DEBUG  ==============================

        private void BtnSnap_Click(object sender, RoutedEventArgs e)
        {
            ChooseLed(0, 1);
        }

        private void BtnBoom_Click(object sender, RoutedEventArgs e)
        {
            ChooseLed(1,0);
        }

        private void BtnBlink_Click(object sender, RoutedEventArgs e)
        {
            BlinkLeds();
        }

        private void BtnRelease_Click(object sender, RoutedEventArgs e)
        {
            busyFlg = false;
            ReleaseLeds();
        }

        #endregion

        #region =============================== UDP ==============================

        private async void Listen()
        {
            listenerSocket1 = new DatagramSocket();
            listenerSocket1.MessageReceived += MessageReceived;
            await listenerSocket1.BindServiceNameAsync(recievePort1);

            listenerSocket2 = new DatagramSocket();
            listenerSocket2.MessageReceived += MessageReceived;
            await listenerSocket2.BindServiceNameAsync(recievePort2);
        }

        private async void Send(string str)
        {
            IOutputStream outputStream;
            string localIPString = "255.255.255.255";
            IPAddress localIP = IPAddress.Parse(localIPString);
            string subnetMaskString = "255.255.255.0";
            IPAddress subnetIP = IPAddress.Parse(subnetMaskString);
            HostName remoteHostname = new HostName(localIP.ToString());
            outputStream = await listenerSocket1.GetOutputStreamAsync(remoteHostname, sendPort);

            using (DataWriter writer = new DataWriter(outputStream))
            {
                writer.WriteString(str);
                await writer.StoreAsync();
            }


        }

        //private object GetBroadcastAddress(IPAddress localIP, IPAddress subnetIP)
        //{
        //    throw new NotImplementedException();
        //}

        async void MessageReceived(DatagramSocket socket, DatagramSocketMessageReceivedEventArgs args)
        {
            DataReader reader = args.GetDataReader();
            uint len = reader.UnconsumedBufferLength;
            string msg = reader.ReadString(len);

            string remoteHost = args.RemoteAddress.DisplayName;
            reader.Dispose();

            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                Log(msg);
                
                ParseMessage(msg);
            });

        }


        #endregion

        #region =============================== TOOL ==============================

        private void PlaySound(int index)
        {
            for (int i = 0; i < Sounds.Length; i++)
            {
                Sounds[i].PlaybackSession.Position = new TimeSpan(0, 0, 0);
                Sounds[i].Pause();
            }
            Sounds[index].Play();
        }

        private void BlinkLeds()
        {
            blinkTimer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(300) };
            blinkTimer.Start();
            bool flg = false;
            int counter = 0;

            blinkTimer.Tick += (s, e) =>
            {
                if (flg)
                {
                    relayPins[0].Write(GpioPinValue.High);
                    relayPins[1].Write(GpioPinValue.High);
                    flg = false;
                }
                else
                {
                    relayPins[0].Write(GpioPinValue.Low);
                    relayPins[1].Write(GpioPinValue.Low);
                    flg = true;

                }

                if (counter++ > 4)
                {
                    blinkTimer.Stop();
                    ReleaseLeds();
                }
            };
        }

        private void ReleaseLeds()
        {
            if (chosenFlg[0] == true)
            {
                relayPins[0].Write(GpioPinValue.Low); //ON
                relayPins[1].Write(GpioPinValue.High); //OFF
            }
            else if (chosenFlg[1] == true)
            {
                relayPins[1].Write(GpioPinValue.Low); //ON
                relayPins[0].Write(GpioPinValue.High); //OFF
            }
        }

        private void Log(string str)
        {
            lstLog.Items.Add(DateTime.Now + " - " + str);
        }

        #endregion



    }
}
